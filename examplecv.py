import numpy as np
import cv2
import Tkinter as tk
import Image, ImageTk
from matplotlib import pyplot as plt
import sys

#Set up GUI
window = tk.Tk()  #Makes main window
window.wm_title("Cagri GENC Image Proccesing Application")
window.config(background="#000000")

#Graphics window
imageFrame = tk.Frame(window, width=600, height=500)
imageFrame.grid(row=0, column=1, padx=10, pady=2)

#Capture video frames
lmain = tk.Label(imageFrame)
lmain.grid(row=0, column=0)
cap = cv2.VideoCapture(0)
def show_frame():
    _, frame = cap.read()
    frame = cv2.flip(frame, 1)
    cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
    img = Image.fromarray(cv2image)
    imgtk = ImageTk.PhotoImage(image=img)
    lmain.imgtk = imgtk
    lmain.configure(image=imgtk)
    lmain.after(10, show_frame) 

def takephoto():
    _, frame1 = cap.read()
    cv2.imshow('img1',frame1)
def blackwhite():
    _, frame2 = cap.read()
    cv2image1 = cv2.cvtColor(frame2, cv2.COLOR_BGR2GRAY)
    img1 = Image.fromarray(cv2image1)
    cv2.imshow('img1',cv2image1)
def sobel():
    scale = 1
    delta = 0
    ddepth = cv2.CV_16S
    
    _, img3 = cap.read()
    img_temp=img3
    edges = cv2.Canny(img_temp,100,100)
    hsv = cv2.cvtColor(img3, cv2.COLOR_BGR2HSV)
    
    lower_red = np.array([30,150,50])
    upper_red = np.array([255,255,180])
    
    mask = cv2.inRange(hsv, lower_red, upper_red)
    res = cv2.bitwise_and(img3,img3, mask= mask)
    img3 = cv2.GaussianBlur(res,(1,1),0)
    gray = cv2.cvtColor(img3,cv2.COLOR_BGR2GRAY)
    
    # Gradient-X
    grad_x = cv2.Sobel(gray,ddepth,1,0,ksize = 3, scale = scale, delta = delta,borderType = cv2.BORDER_DEFAULT)
    #grad_x = cv2.Scharr(gray,ddepth,1,0)

    # Gradient-Y
    grad_y = cv2.Sobel(gray,ddepth,0,1,ksize = 3, scale = scale, delta = delta, borderType = cv2.BORDER_DEFAULT)
    #grad_y = cv2.Scharr(gray,ddepth,0,1)

    abs_grad_x = cv2.convertScaleAbs(grad_x)   # converting back to uint8
    abs_grad_y = cv2.convertScaleAbs(grad_y)

    dst = cv2.addWeighted(abs_grad_x,0.5,abs_grad_y,0.5,0)
    #dst = cv2.add(abs_grad_x,abs_grad_y)


    plt.subplot(2,2,1),plt.imshow(img_temp,cmap = 'gray')   
    plt.title('Original'), plt.xticks([]), plt.yticks([])
    plt.subplot(2,2,2),plt.imshow(edges,cmap = 'gray')
    plt.title('Canny'), plt.xticks([]), plt.yticks([])
    plt.subplot(2,2,3),plt.imshow(gray,cmap = 'gray')
    plt.title('Gray'), plt.xticks([]), plt.yticks([])
    plt.subplot(2,2,4),plt.imshow(dst,cmap = 'gray')
    plt.title('DST'), plt.xticks([]), plt.yticks([])
 
    plt.show()
def facedetect():
    face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
    eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')

    _, imgf = cap.read()
    gray1 = cv2.cvtColor(imgf, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray1, 1.3, 5)
    for (x,y,w,h) in faces:
        img = cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = img[y:y+h, x:x+w]
        eyes = eye_cascade.detectMultiScale(roi_gray)
        for (ex,ey,ew,eh) in eyes:
            cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)

    cv2.imshow('img',imgf)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
#Slider window (slider controls stage position)
sliderFrame = tk.Frame(window, width=100, height=100)
sliderFrame.grid(row = 0, column=0, padx=10, pady=2)
tk.Button(sliderFrame,text="Foto �ek",command=takephoto).grid(row=0,column=0)
tk.Button(sliderFrame,text="S-B Yap",command=blackwhite).grid(row=1,column=0)
tk.Button(sliderFrame,text="Sobel",command=sobel).grid(row=2,column=0)
tk.Button(sliderFrame,text="Face Detect",command=facedetect).grid(row=3,column=0)

show_frame()  #Display 2
window.mainloop()  #Starts GUI
